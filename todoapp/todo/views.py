from django.contrib.auth.models import User
from rest_framework import viewsets
from todoapp.todo.serializers import UserSerializer, TaskSerializer
from todoapp.todo.models import Tasks

class UserListView(viewsets.ReadOnlyModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class TasksListView(viewsets.ModelViewSet):
    """
    API endpoint that allows todos to be added and listed
    """
    http_method_names = ['get','post']
    serializer_class = TaskSerializer

    def get_queryset(self):
         """
         Override method to get todo's only current logged user and sort by created date
         """
         user = self.request.user
         return Tasks.objects.filter(owner=user.username).order_by('-created_date')

    def perform_create(self, serializer):
        """
        This method adding current user to serializer, owner for a new todo's
        """
        serializer.save(owner=self.request.user)
