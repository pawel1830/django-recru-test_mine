from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework.authtoken.models import Token
from rest_framework import status
from models import Tasks
from serializers import TaskSerializer

# Create your tests here.
class TasksTest(TestCase):
    def setUp(self):
        """
        Create new objects, only for test
        """
        User.objects.create(username='testing', password='testing')
        self.user  = User.objects.get(username='testing')
        self.token = Token.objects.create(user=self.user)
        Tasks.objects.create(title="test1",details="first element",owner=self.user)
        Tasks.objects.create(title="test2", details="second element", owner=self.user)

    def testModel(self):
        """
        Testing getting data from database and represent as django model
        :return:
        """
        todo = Tasks.objects.get(title="test1")
        self.assertEqual(
            todo.__repr__(), "title=test1 details=first element")

    def testNoAuthorization(self):
        """
        Testing trying get data from api, without login
        :return:
        """
        client = APIClient()
        response = client.get('/todos/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def testGetAllObjects(self):
        """
        Testing getting data from api and validating them
        :return:
        """

        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = client.get('/todos/')
        todos = Tasks.objects.filter(owner=self.user.username).order_by('-created_date')
        serializer = TaskSerializer(todos,many=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        #This is a 'hacks' because I want to validate only a data without pagination
        self.assertEqual(response.data['results'], serializer.data)
        client.logout()

    def testPostObject(self):
        """
        Testing sending data to api and verifying them in the database
        :return:
        """
        client = APIClient()
        client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = client.post('/todos/', {'title': 'new todo','details': 'my new detail about todo'}, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        client.logout()
        todo = Tasks.objects.get(title="new todo")
        self.assertEqual(
            todo.__repr__(), "title=new todo details=my new detail about todo")
