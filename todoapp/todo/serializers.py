from django.contrib.auth.models import User, Group
from rest_framework import serializers
from todoapp.todo.models import Tasks

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email')


class TaskSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')
    class Meta:
        model = Tasks
        fields = ('id', 'title', 'details', 'image','owner','created_date')

