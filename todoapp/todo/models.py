from django.db import models
from django.contrib.auth.models import User


class Tasks(models.Model):
    """
    This class represent Tasks table in django model
    """
    title = models.CharField(max_length=128)
    details = models.TextField()
    image = models.ImageField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    owner = models.ForeignKey(User,to_field='username', related_name='tasks_r', on_delete=models.CASCADE)
    class Meta:
        db_table = 'Tasks'
    def __repr__(self):
        return "title=%s details=%s" % (self.title,self.details)