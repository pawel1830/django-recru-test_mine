from django.conf.urls import url, include
from rest_framework import routers
from django.contrib import admin
from todoapp.todo import views

router = routers.DefaultRouter()
router.register(r'users', views.UserListView)
router.register(r'todos', views.TasksListView, base_name='TODO')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', admin.site.urls),
]