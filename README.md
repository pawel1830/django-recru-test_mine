# django + django-rest-framework recruitment tasks;
 
## Overview

We are going to make the todo application; As backend developer you will be responsible
for creating the REST API; 

Imagine that the REST API will be used in the web application and also mobile applications;
We want to create on backend for all of the clients;

You have working set up of the django project - use this as a code base 
for your project; The files you will modify are mainly in the `todoapp.todo` package;

Do not waste your time on setup, eg.: database (sqlite3 is perfectly enough);
Do not integrate this with any external static files storage (eg.: aws s3), use standard
 django media files setup;

## Tasks

### General guidelines

* responses should be in json;
* user object - use default django or create custom model for that; user here is not important
so do not spend a lot of time trying to figure out the attributes; 
* if you will set up the AUTHENTICATION - you will need a token for the tests; you can provide
some django management command for that - or use anything that suits your needs;

### LIST endpoint

On the GET `/todos/` endpoint we would like to get the todos for the given user;
Sorted from newest to oldest; with pagination;

The Todo object should have following attributes in this response:

* id: required, INT
* title: required, STRING(128)
* details required, TEXT
* image - optional, image object (null if not set)


### CREATE endpoint

On The POST `/todos/` we would like to create a todo object for a given user;

The parameters there are:

* title: required, STRING(128)
* details required, TEXT()
* image - optional, image object


### AUTHENTICATION

Use django-rest-framework token authentication for authenticate the requests
and get the user details; If you will be having problems with making the API 
aware of the user context - please skip the user parts in above tasks; so - 
LIST will list all of the todos; and POST will create a todo object without
the user context;

### TESTS

Write simple tests which test both of the endpoints above;

## Rules

* please do not add anything new to the `requirements.txt` - you have everything needed in place;
* write your code in the `todo` application in this repository;

## Help

This links are your friends during development:

* [Official django documentation](https://docs.djangoproject.com/en/1.11/)
* [Official django-rest-framework documentation](http://www.django-rest-framework.org/)


Do not forget about migrations;

## Submitting the solution

If your are done - just create a pull requests in this repository; The PR name should be
your initials with `-solution` suffix; So lets say I my name is John Rambo, the PR should be named: `jr-solution`;
If such PR already exists - use next character of your surname: `jra-solution`;